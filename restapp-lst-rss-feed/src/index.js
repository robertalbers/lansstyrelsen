(function () {
   'use strict';

   var router = require('router');

   //Utils
   var resourceLocatorUtil = require('ResourceLocatorUtil'),
       searchUtil = require('SearchUtil'),
       propertyUtil = require('PropertyUtil'),
       sortBuilder = require('SortBuilder'),
       searchFactory = require('SearchFactory'),
       nodeTreeUtil = require('NodeTreeUtil'),
       endecUtil = require('EndecUtil'),
       locale = java.util.Locale,
       simpleDateFormat = java.text.SimpleDateFormat;

   var today = new Date(),
       latestWeek = new Date();

   latestWeek.setDate(latestWeek.getDate() - 7);

   var todaysDate = ("0" + today.getDate()).slice(-2),
       todaysMonth = today.getMonth() + 1,
       todaysYear = today.getFullYear();

   var latestDate = ("0" + latestWeek.getDate()).slice(-2),
       latestMonth = latestWeek.getMonth() + 1,
       latestYear = latestWeek.getFullYear();

   router.get('/lst-eplikt/:lstName', function (req, res) {
       //Parameter send        
       var lstName = req.params.lstName;

       //Decode URL parameter from charachters å,ä,ö        
       var decodeParameter = decodeURIComponent(lstName);

       var lstResolveName = capitalizeLstName(decodeParameter);

       //Define sort options
       var sortFields = new java.util.ArrayList(),
           searchSortField = searchFactory.getSearchSortField('lastpublished', false);

       sortBuilder.addSortField(searchSortField).build();
       sortFields.add(searchSortField);


       //Get Sitepage and Filerepository
       var sitePage = resourceLocatorUtil.getSitePage(),
           fileRepo = resourceLocatorUtil.getFileRepository();

       //Variables defined for search purposes
       var lstId,
           fileRepositoryId,
           filePublicationId;

       //Define what LST we are looking for
       if (sitePage.hasNode(lstResolveName)) {
           lstId = sitePage.getNode(lstResolveName).getIdentifier();
       }
       else {
           lstId = lstResolveName;
       }

       if (fileRepo.hasNode(lstResolveName)) {
           fileRepositoryId = fileRepo.getNode(lstResolveName).getIdentifier();
           //Ändra
           filePublicationId = nodeTreeUtil.getNode(fileRepo, "/Publikationer/" + lstResolveName).getIdentifier();
       }
       else {
           fileRepositoryId = lstResolveName;
           filePublicationId = nodeTreeUtil.getNode(fileRepo, "/Publikationer/" + lstResolveName).getIdentifier();
       }
       //Fetch orgnumber from each LST.
       var orgNumberNode = resourceLocatorUtil.getNodeByIdentifier(lstId),
           orgNumber = propertyUtil.getString(orgNumberNode, 'lstOrgNumber');

       //Search in index - sort by lastpublished
       var searchString = '((metadata.lstEplikt:"Ja" && path:"' + lstId + '") OR (metadata.lstEplikt:"Ja" && path:"' + fileRepositoryId + '") OR (metadata.lstEplikt:"Ja" && path:"' + filePublicationId + '")) AND (lastpublished:[' + latestYear + '-' + latestMonth + '-' + latestDate + 'T00:00:01.999Z TO ' + todaysYear + '-' + todaysMonth + '-' + todaysDate + 'T23:59:59.999Z])',
           searchResult = searchUtil.search(searchString, sortFields, 0, 1000);

       //XML to be displayed
       var xmlItems = "",
           dateFormatPattern = new simpleDateFormat("EEE, dd MMM yyy HH:mm:ss Z", new locale("en"));

       if (searchResult.hasHits()) {
           var hits = searchResult.getHits();

           var i = 0;

           while (hits.hasNext()) {

               i++;

               var hit = hits.next(),
                   itemName = hit.getField('name'),
                   itemURL = hit.getField('url'),
                   itemId = hit.getField('id'),
                   itemMimeType = hit.getField('mimetype'),
                   itemPublisher = 'http://id.kb.se/organisations/SE' + orgNumber,
                   itemLastPublishedDate = hit.getDateField('lastpublished'),
                   itemNumber = hit.getField('metadata.isbnIssnNr'),
                   itemLastCreator = hit.getField('lastpublishedby');

               //Check for itemNumber and match
               var itemNumberXml = "";

               if (itemNumber !== null) {
                   var index = itemNumber.charAt(4);
                   if (index === "-") {
                       itemNumberXml = '<dcterms:issn>' + itemNumber + '</dcterms:issn>';
                   }
                   else {
                       itemNumberXml = '<dcterms:isbn>' + itemNumber + '</dcterms:isbn>';
                   }
               }

               //Get node to get imageUrl
               var itemNode = hit.getNode(),
                   itemImageNode = propertyUtil.getNode(itemNode, 'featuredImage'),
                   itemImageUrl = propertyUtil.getString(itemImageNode, 'URL'),
                   itemImageXml = "";

               if (itemImageUrl !== null) {
                   itemImageXml = '<media:content url="' + itemImageUrl + '" type="text/html"><dcterms:isFormatOf>' + itemImageUrl + '</dcterms:isFormatOf></media:content>';
               }

               //Set itemLastPublished to format pattern
               var itemLastPublishedDateString = dateFormatPattern.format(itemLastPublishedDate);

               //Varje item ska ha mer än nedan - t.ex. kategori                
               xmlItems +=
                   '<item>' +
                   '<guid>' + itemId + '</guid>' +
                   '<link>' + itemURL + '</link>' +
                   '<pubDate>' + itemLastPublishedDateString + '</pubDate>' +
                   '<title>' + endecUtil.escapeXML(itemName) + '</title>' +
                   '<dcterms:format>' + itemMimeType + '</dcterms:format>' +
                   itemNumberXml +
                   '<dcterms:publisher>' + itemPublisher + '</dcterms:publisher>' +
                   '<dcterms:accessRights>restricted</dcterms:accessRights>' +
                   '<dcterms:creator>' + itemLastCreator + '</dcterms:creator>' +
                   itemImageXml +
                   '</item>';
           }
       }
       else {
           xmlItems = 'Inga träffar';
       }
       var xmlResult =
           '<?xml version="1.0" encoding="UTF-8"?>' +
           '<rss version="2.0" xmlns:georss="http://www.georss.org/georss" xmlns:media="http://search.yahoo.com/mrss/" xmlns:dcterms="http://purl.org/dc/terms/"> ' +
           '<channel>' +
           '<title>Pliktleverans av elektroniskt material från Länsstyrelsen i ' + lstResolveName + '</title>' +
           '<link>https://lansstyrelsen.se/' + endecUtil.getBaseSubstitute(lstResolveName.toLowerCase()) + '</link>' +
           '<language>sv</language>' +
           '<description>Elektroniskt material från https://lansstyrelsen.se/' + endecUtil.getBaseSubstitute(lstResolveName) + '</description>' +
           xmlItems +
           '</channel>' +
           '</rss>';

       //If updated - use content type XML     
       res.set('content-type', 'text/xml');
       //res.type('application/xml');
       res.send(xmlResult);
   });

   //Functions used
   function capitalizeLstName(municipality) {
       return municipality[0].toUpperCase() + municipality.slice(1);
   }

}());